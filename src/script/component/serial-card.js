"use strict";

import Emitter from '../../script/component/port/emitter.js'
import Receiver from '../../script/component/port/receiver.js'

import baseFcn from '../../script/asset/base-fcn.js'

import cmdForm from '../../component/cmd-form.vue'

var self = {
  components: {
    "cmd-form": cmdForm
  },
  props: {
    path: {
      type: String,
      default: ""
    },
  },
  data: function () {
    return {
      status: {           // card 狀態
        isSelected: false,  // 已選擇
        isPortOpen: false,    // COM 是否開啟
      },

      com: {              // serialport 相關參數
        path: "COM3",       // 路徑
        obj: null,          // serialport 物件

        emitter: new Emitter(),      // 發射器
        receiver: new Receiver(),    // 接收器

        parser: {           // 解析器設定
          type: "InterByteTimeout",           // 種類，從 list 選擇
          list: [             // 全部清單
            {
              type: "NONE",
              description: "不使用解析器",
            },
            {
              type: "Regex",
              description: "根據「正則表達式」拆分回傳數據",
            },
            {
              type: "ByteLength",
              description: "收到指定「byte 數量」後回傳數據",
            },
            {
              type: "Readline",
              description: "收到指定「換行符」後回傳「指定編碼」字符串",
            },
            {
              type: "Delimiter",
              description: "收到指定「換行符」後回傳數據",
            },
            {
              type: "InterByteTimeout",
              description: "在資料停止傳輸於「指定時間間隔」後回傳數據",
            },
          ],
          option: {
            length: 1,        // ByteLength

            encoding: "utf8", // Readline
            delimiter: "\n",  // Delimiter、Readline

            interval: 50,     // InterByteTimeout
            maxBufferSize: 65536,

            regex: "(.|\n)+?", // Regex
          },
        },

        option: {           // 預設設定值
          baudRate: 115200,
          highWaterMark: 65536,
          dataBits: 8,
          stopBits: 1,
          parity: "none",
          autoOpen: false,    // 是否自動開啟 port

          sendClear: false,   // 發送後自動清空
          autoScroll: true,   // 貼底自動捲動
          isSendNum: false,   // 是否發送純數值
          hexNum: false,      // 是否以 16 進位顯示
          startChar: "",      // 起始符
          endChar: "",        // 結束符
        },
      },

      panel: {            // 面板狀態
        showOption: false,  // 顯示 option
        showCmds: false,    // 顯示命令列
      },

      input: {            // 輸入框
        timestamp: 0,
        val: "",
      },

      textarea: {         // 接收訊息顯示區
        display: {          // 顯示方式
          showTime: false,  // 是否顯示時間戳記
          dateFormat: "HH:mm:ss"
        },
        style: {
          fontSize: {       // 文字大小
            val: 14, unit: "px"
          },
          lineHeight: {     // 行高
            val: 2, unit: ""
          },
          letterSpacing: {  // 字距
            val: 1.5, unit: "px"
          }
        }
      },

      receiceData: [      // 目前接收資料
      ],

      debounce: {
        settingEvent: null
      },
    }
  },
  created: function () {
    //console.log("path : ", this.path);
    this.com.path = this.path;

    // 建立 debounce function
    this.debounce.settingEvent = baseFcn.debounce(cardSetting => {
      this.$emit("setting", cardSetting)
    }, 1000)

    if (baseFcn.isWeb()) return

    // 自動載入 card 記錄檔
    this.$loadRecord(`card-setting`).then(cardSetting => {
      //console.log("[ load card setting ] setting : ", cardSetting);

      if (cardSetting[this.path])
        this.setCardSetting(cardSetting[this.path])

      // 建立 serialport 物件
      this.createPort()
    }).catch(err => {
      this.showTips(`${this.path} Card 載入設定檔錯誤 : ${err}`, "err")
    })
  },
  mounted: function () {
  },
  beforeDestroy: function () {
    if (this.com.obj && this.com.obj.isOpen && this.com.obj.close)
      this.com.obj.close();

    if (this.com.obj && this.com.obj.removeAllListeners)
      this.com.obj.removeAllListeners();
  },
  watch: {
  },
  methods: {
    // 卡片開關
    switchPort: function () {
      if (baseFcn.isWeb()) {
        this.status.isPortOpen = !this.status.isPortOpen;
        return
      }

      // 開啟 port
      if (!this.status.isPortOpen)
        return this.com.obj.open()


      // 關閉 port
      if (this.com.obj && this.com.obj.isOpen) {
        this.com.obj.close();
      }
    },

    // 開啟 port
    createPort: function () {
      let SerialPort = global.SerialPort;
      if (!SerialPort)
        return this.showTips("serialport 模組載入異常", "err")

      let portObj = new SerialPort(this.com.path, this.com.option);
      this.com.obj = portObj

      // 設定 receiver
      this.com.receiver.setPort(portObj);
      this.setParser()

      this.com.receiver.on("data", (data) => { // 收到資料
        console.log(`[ ${this.path} Card receiver ] data : `, data);

        // 將 buffer 轉為 string 儲存
        let string = ""

        // 以 16 進位顯示
        if (this.com.option.hexNum) {
          if (data.length) {
            // 避免較大的數值變為 FFFD
            data.forEach(item => string += String.fromCharCode(item))
          } else {
            string = String.fromCharCode(data)
          }
        } else {
          if (data.length) {
            // 避免中文變亂碼
            string = data + "";
          } else {
            string = String.fromCharCode(data)
          }
        }

        this.receiveData(string)
      })

      portObj.on('open', () => {
        this.status.isPortOpen = true;
        this.showTips("Port 開啟")
      })
      portObj.on('close', () => {
        this.status.isPortOpen = false;
        this.showTips("Port 關閉")
      })
      portObj.on('error', (err) => {
        this.showTips(`Port 發生錯誤 : ${err}`, "err")
        console.log(`[ ${this.path} Card ] port err : `, err);
      })
    },
    setParser: function () {
      let recRes = this.com.receiver.setParser({
        type: this.com.parser.type, ...this.com.parser.option,
      });
      recRes.msg !== "" && this.showTips(recRes.msg)
    },

    // 發送數據
    sendEvent: function (val = null) {
      if (!this.com.obj)
        return this.showTips("未生成 Port 物件", "err")
      if (!this.com.obj.isOpen)
        return this.showTips("Port 未開啟", "err")


      // val 為空表示輸入框送出
      if (!val) {
        if (this.input.val === "")
          return this.showTips("數值為空", "err")

        this.writeData(this.input.val)
        this.input.timestamp = new Date() / 1
        return
      }

      // 外部呼叫傳值
      if (val === "")
        return this.showTips("數值為空", "err")
      this.writeData(val)
    },
    writeData(val) {
      // 純數值發送
      if (this.com.option.isSendNum) {
        let { status, msg, data } = baseFcn.checkSpaceStrArray(val)

        // 發生錯誤
        if (status === "err")
          return this.showTips(msg, status)

        this.com.obj.write(data)
        return
      }

      this.com.obj.write(val)
    },

    // 儲存接收數據
    receiveData: function (data) {
      if (this.com.option.autoScroll && this.isScrollBottom(this.$refs.dataTextarea))
        setTimeout(() => this.scrollTextarea(), 50)

      this.receiceData.push({
        timestamp: baseFcn.getUnix(), data, type: "data"
      })

      if (this.receiceData.length > 5000)
        this.receiceData.splice(0, 4000)
    },
    // 在資料顯示區顯示系統提示訊息
    showTips: function (msg, type = "") {
      //console.log(`[ showTips ] `, msg);

      if (this.com.option.autoScroll && this.isScrollBottom(this.$refs.dataTextarea))
        setTimeout(() => this.scrollTextarea(), 50)

      let data = ``;
      switch (type) {
        case "err": case "warn":
          data = `<${type}>⊙ ${msg}</${type}>`; break;

        default: {
          data = `⊙ ${msg}`
        }
      }

      this.receiceData.push({
        timestamp: baseFcn.getUnix(), data, type: "tip"
      })
    },
    formatTimestamp: function (stamp) {
      return baseFcn.formatUnix(stamp * 1000, this.textarea.display.dateFormat)
    },

    // 設定卡片設定
    setCardSetting: function (setting) {
      try {
        let cSetting = Object.copy(setting)
        delete cSetting.path

        this.com.parser.type = cSetting.com.parser.type
        this.com.parser.option = cSetting.com.parser.option

        if (!Object.isEmpty(cSetting.com.option))
          this.com.option = cSetting.com.option

        this.textarea.display = cSetting.textarea.display
        this.textarea.style = cSetting.textarea.style

        if (!Object.isEmpty(cSetting.panel))
          this.panel = cSetting.panel
      } catch (err) {
        console.warn(`[ ${this.path} Card ] 載入紀錄設定檔發生錯誤 : `, err)
      }
    },
    // 取得卡片設定
    getCardSetting: function () {
      return this.cardSetting
    },
    // 刪除卡片
    deleteCard: function () {
      this.$emit("delete", this.path)
    },
    selectCard: function () {
      this.status.isSelected = true
    },
    blurCard: function () {
      this.status.isSelected = false
    },

    // 清空接收訊息
    clearReceiceData: function () {
      this.receiceData = [];
      this.receiceData.length = 0;
    },
    // textarea 滾動到底
    scrollTextarea: function () {
      try {
        this.setDomScroll(this.$refs.dataTextarea)
      } catch (err) { }
    },

    // 控制 DOM 捲動條
    setDomScroll: function (dom, pos = - 1) {
      if (pos === -1) {
        dom.scrollTop = 999999;
        return
      }

      dom.scrollTop = pos;
    },
    // DOM 是否捲動到底
    isScrollBottom: function (dom) {
      let height = dom.clientHeight;
      let scrollTop = dom.scrollTop;
      let scrollHeight = dom.scrollHeight;

      return (height + scrollTop) > (scrollHeight * 0.9)
    },
    // 清空 DOM 樣式
    clearStyle: function (key) {
      if (this.$refs[key])
        this.$refs[key].style = ""
    },
  },
  computed: {
    // textarea 顯示接收資料
    textareaDatas: function () {
      return this.receiceData.map(oData => {
        let data = Object.copy(oData)

        if (data.type === "tip") return data

        // HEX 顯示
        if (this.com.option.hexNum) {
          data.data = data.data.split("").map(char => {
            /*console.log("char : ", char)
            if (char.charCodeAt)
              console.log(`code : `, char.charCodeAt());//*/

            return baseFcn.padLeft(char.charCodeAt().toString(16), 2).toUpperCase()
          }).join(" ")
        }

        return data
      })
    },

    // textarea 自訂樣式
    textareaStyle: function () {
      let style = {};

      Object.keys(this.textarea.style).forEach((key) => {
        let obj = this.textarea.style[key]
        style[key] = `${obj.val}${obj.unit}`
      })

      return style
    },

    // 目前選擇 parser 說明
    parserDescription: function () {
      let type = this.com.parser.type

      let parser = this.com.parser.list.find(item => {
        return item.type === type
      })
      if (!parser) return ""
      return parser.description
    },


    // 卡片設定
    cardSetting: function () {
      let setting = {
        path: this.com.path,
        com: {
          parser: {},
          option: {}
        },
        textarea: {},
        panel: {},
      }

      // com option
      setting.com.option = Object.copy(this.com.option)

      // parser option
      let parser = Object.copy(this.com.parser)
      delete parser.list
      setting.com.parser = parser

      // textarea option
      let textareaKeys = ["display", "style"]
      textareaKeys.forEach(key => {
        setting.textarea[key] = Object.copy(this.textarea[key])
      })

      // panel option
      setting.panel = Object.copy(this.panel)

      this.debounce.settingEvent(setting)
      return setting
    },

    // 已純數值發送時，檢查是否有錯誤
    sendNumStatus: function () {
      if (!this.com.option.isSendNum)
        return { status: "", msg: "" }

      return baseFcn.checkSpaceStrArray(this.input.val)
    },
  }
}

export default self