/**
* serialport 接收器
* 
* 接收命令
*/

import baseFcn from "../../../script/asset/base-fcn.js"


var receiver = function (portObj = null, options = {}) {
  let port = portObj    // serialport 物件
  let parser = null     // serialport 解析器

  let emitter = null    // EventEmitter 物件

  let buffer = []       // 接收資料暫存，timeout 後才完成


  init()

  // --- fcn ---

  function init() {
    if (baseFcn.isWeb()) return

    if (!global.EventEmitter) {
      console.error(`[ receiver ] 無法載入 EventEmitter`);
      return
    }

    EventEmitter = global.EventEmitter;
    emitter = new EventEmitter();

    setEvent()
  }

  // 建立 emitter 事件
  function setEvent() {
    /*emitter.on("sent", (cmd) => {
    })//*/
  }

  return {
    // 傳入 serialport 物件
    setPort: function (portIn) {
      port = portIn;
    },
    // 建立解析器
    setParser: function (option = null) {
      if (!port)
        return { status: "err", msg: "尚未建立 Port 物件" }

      if (!option)
        return { status: "err", msg: "請輸入設定值" }

      let SerialPort = global.SerialPort;
      if (!SerialPort)
        return { status: "err", msg: "serialport 模組載入異常" }

      // 初始化
      port.removeAllListeners("data");

      switch (option.type) {
        case "NONE": {
          port.on("data", data => emitter.emit("data", data))
          break;
        }

        case "ByteLength": case "Readline": case "Delimiter": case "Regex": {
          let ParserClass = SerialPort.parsers[option.type]
          if (!ParserClass)
            return { status: "err", msg: `serialport 載入 ${option.type} parser 異常` }

          parser = port.pipe(new ParserClass(option))
          parser.on("data", data => emitter.emit("data", data))
          break;
        }

        case "InterByteTimeout": {
          let ParserClass = require('@serialport/parser-inter-byte-timeout')
          if (!ParserClass)
            return { status: "err", msg: `serialport 載入 ${option.type} parser 異常` }

          parser = port.pipe(new ParserClass(option))
          parser.on("data", data => emitter.emit("data", data))
          break;
        }

        default: {
          port.on("data", data => emitter.emit("data", data))
          return { status: "err", msg: `未定義的 parser : ${option.type}` }
        }
      } // switch

      return { status: "sus", msg: "" }
    },
    // 刪除 serialport 物件
    destroyPort: function () {
      port = null; parser = null;
    },

    on: function (event, callback) {
      emitter.on(event, callback)
      return emitter
    },
    emit: function (event, arg = undefined) {
      emitter.emit(event, arg)
      return emitter
    },
  }
}

export default receiver