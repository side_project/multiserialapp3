/**
 * serialport 發射器
 * 
 * 間隔指定時間發出的排程中命令
 */

import baseFcn from "../../../script/asset/base-fcn.js"


var emitter = function (portObj = null, options = {}) {
  let port = portObj    // serialport 物件

  let emitter = null    // EventEmitter 物件

  let enable = options.enable || true;     // 開始發送
  let interval = options.interval || 100;  // 命令間隔
  let intervalTimer = 0;                   // 紀錄時間戳記

  let timeout = options.timeout || 1000;   // 超時
  let timeoutTimer = null;                 // 超時計時器

  let queueCmds = [];                      // 等待發送的命令

  init();
  processCmds();

  // --- fcn ---

  function init() {
    if (baseFcn.isWeb()) return

    if (!global.EventEmitter) {
      console.error(`[ emitter ] 無法載入 EventEmitter`);
      return
    }

    EventEmitter = global.EventEmitter;
    emitter = new EventEmitter();

    setEvent()
  }

  // 處理命令發送細節
  function processCmds() {
    setInterval(() => {
      // 不須發送命令
      if (!enable || queueCmds.length === 0) {
        //console.log("[ emitter ] 不須發送命令");
        return
      }

      // port 不可用
      if (!port || !port.isOpen) {
        //console.log("[ emitter ] port 不可用");
        return
      }

      // 等待回應，不可發送命令
      if (timeoutTimer !== null) {
        //console.log("[ emitter ] 等待回應");
        return
      }

      // 偵測時間差     
      let ms = new Date() / 1
      if (Math.abs(ms - intervalTimer) >= interval || intervalTimer === 0) {
        let cmd = queueCmds.shift()
        port.write(cmd.cmd)
        //console.log("[ emitter ] cmd write : ", cmd.cmd);

        emitter.emit("sent", cmd)

        intervalTimer = ms
      }
    }, 10)
  }


  function clearTimeoutIimer() {
    if (timeoutTimer === null) return

    clearTimeout(timeoutTimer)
    timeoutTimer = null;//*/
  }

  // 建立 emitter 事件
  function setEvent() {
    // 回應解析成功，清除超時計時
    emitter.on("data", (data) => {
      clearTimeoutIimer()
    })
  }

  // 增加等待發送資料
  function pushQueue(cmd) {
    queueCmds.push(cmd)
  }

  return {
    // 傳入 serialport 物件
    setPort: function (portIn) {
      port = portIn;
    },
    // 刪除 serialport 物件
    destroyPort: function () {
      port = null;
    },

    on: function (event, callback) {
      emitter.on(event, callback)
      return emitter
    },
    emit: function (event, arg) {
      emitter.emit(event, arg)
      return emitter
    },
  }
}

export default emitter