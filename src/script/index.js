"use strict";

import baseFcn from './asset/base-fcn.js'

import serialCard from '../component/serial-card.vue'

Quasar.lang.set(Quasar.lang.zhHant)


var vueDatas = {
  status: {
    isReady: false
  },

  // 選單相關功能
  menu: {
    com: {
      show: false,
      list: [
        {
          path: "COM1",
          manufacturer: "",
          serialNumber: "",
          locationId: "",
          vendorId: "",
          productId: "",
        },
        {
          path: "COM2",
          manufacturer: "DOG",
          serialNumber: "",
          locationId: "",
          vendorId: "",
          productId: "",
        },
        {
          path: "COM3",
          manufacturer: "Coooooodfish",
          serialNumber: "",
          locationId: "",
          vendorId: "",
          productId: "",
        },
      ]
    },

    option: { // 全域設定
      show: false,
      items: {
        storeFile: {    // 管理儲存檔案
          selectCard: "", // 選擇卡片
          card: [],       // 卡片設定檔

          selectCmd: "",  // 選擇命令集
          cmd: [],        // 命令集
        },
      },
    },
  },

  // 現有卡片
  cards: {
    list: {
      "COM3": {
        timestamp: 0,
      },
    }
  }
}

var svm = new Vue({
  el: '.screen',
  data: vueDatas,
  components: {
    "serial-card": serialCard
  },
  created: function () {
    // 註冊快捷鍵
    this.registerHotKey()

    if (baseFcn.isWeb()) return

    // 紀錄版本號
    let ver = require('electron').remote.app.getVersion();
    document.title = document.title + ver;

    // 清空 card
    this.cards.list = {}

    this.loadNodeMods().then(() => {
      console.log("[ loadNodeMods ] 載入 node 模組成功");

      global.SerialPort.list().then(list => {
        this.menu.com.list = list;
      })

      this.initEvent()
    }).catch(err => {
      console.error("[ loadNodeMods ] 載入 node 模組失敗 : ", err);

      this.$q.notify({
        type: "negative", position: "top",
        message: `載入 node 模組失敗`
      })
    })
  },
  mounted: function () {
    // 載入完成
    setTimeout(() => {
      this.status.isReady = true
    }, 1000)
  },
  watch: {
  },
  methods: {
    // 切換卡片
    switchCard: function (path) {
      if (this.isCardExisting(path)) {
        this.deleteCard(path)
        return
      }

      this.createCard(path)
    },
    // 生成卡片
    createCard: function (path) {
      this.$set(this.cards.list, path, {
        timestamp: baseFcn.getUnix()
      })
    },
    deleteCard: function (path) {
      this.$delete(this.cards.list, path);
    },
    // 卡片是否已經存在
    isCardExisting: function (path) {
      if (this.cards.list && this.cards.list[path])
        return true
      return false
    },

    // 卡片設定變更事件
    cardSettingEvent: function (setting) {
      if (baseFcn.isWeb()) return

      //console.log(`[ cardSettingEvent ] `, setting);

      this.$loadRecord("card-setting").then(cardSetting => {
        cardSetting[setting.path] = setting;
        return this.$saveRecord("card-setting", cardSetting)
      }).then(res => {
        console.log(`[ cardSettingEvent ] ${setting.path} 儲存成功`)
      }).catch(err => {
        console.error(`[ cardSettingEvent ] ${setting.path} 儲存錯誤 : `, err)
      })
    },

    // 畫面點擊事件
    screenClick: function () {
      // 取消選取所有卡片
      if (this.$refs["serialCard"] && this.$refs["serialCard"].length)
        this.$refs["serialCard"].forEach((card) => {
          card.blurCard()
        })
    },

    // 初始化
    initEvent: function () {
      setInterval(() => {
        // 持續更新 COM List
        global.SerialPort.list().then(list => {
          svm.$set(this.menu.com, "list", list)
        });

        // 持續更新卡片設定檔
        this.$loadRecord(`card-setting`).then(cardSetting => {
          this.menu.option.items.storeFile.card = Object.keys(cardSetting)
        }).catch(err => {
          console.warn("[ loadRecord ] card-setting : ", err)
        })

        // 持續更新命令集
        this.$loadRecord(`cmd-list`).then(cmdList => {
          this.menu.option.items.storeFile.cmd = Object.keys(cmdList)
        }).catch(err => {
          console.warn("[ loadRecord ] cmd-list : ", err)
        })
      }, 1000)


    },

    // 載入 node 模組
    loadNodeMods: function () {
      return new Promise((resolve, reject) => {
        if (baseFcn.isWeb())
          return reject("網頁環境")

        try {
          global.SerialPort = require("serialport");
          global.EventEmitter = require("events");
          global.JsonStorage = require("electron-json-storage");
          global.FileSys = require("fs");
          global.Electron = require("electron");
        } catch (err) {
          return reject(err)
        }

        resolve()
      })
    },

    registerHotKey: function () {
      // 監聽 F1 ~ F12
      window.addEventListener('keydown', (e) => {
        if (event.keyCode < 112 || event.keyCode > 123) return

        let keyIndex = e.keyCode - 112; // 映射至 0~11

        // 所有卡片
        if (this.$refs["serialCard"])
          this.$refs["serialCard"].forEach(card => {
            // 沒選取，不動作
            if (!card.status.isSelected) return

            card.$refs["cmd-form"].sendCmdByIndex(keyIndex)
          })

        //e.preventDefault && e.preventDefault()
        return false
      })

      // 開啟 devTool
      Mousetrap.bind(["command+d", "ctrl+d"], (e) => {
        if (baseFcn.isWeb()) return

        let mainWindow = global.Electron.remote.getCurrentWindow();
        mainWindow.webContents.openDevTools();
        
        e.preventDefault && e.preventDefault()
        return false
      })
    },

    // 刪除指定卡片記錄
    popupDeleteCardRecord: function (record) {
      this.$q.notify({
        message: `確認刪除 ${record} 卡片記錄檔？`, progress: true,
        actions: [
          {
            label: "確認", color: "yellow", icon: "check", handler: () => {
              this.$loadRecord("card-setting").then(cardSetting => {
                delete cardSetting[record]
                return this.$saveRecord("card-setting", cardSetting)
              }).then(res => {
                this.$q.notify({
                  message: `刪除 ${record} 卡片記錄檔成功`,
                  position: "top", type: "positive",
                })
              }).catch(err => {
                this.$q.notify({
                  message: `刪除 ${record} 卡片記錄檔錯誤 : `,
                  position: "top", err, type: "negative",
                })
              })
            }
          },
          { label: "取消", color: "white", icon: "close" }
        ]
      })
    },

    // 刪除指定命令集
    popupDeleteCmdRecord: function (cmd) {
      this.$q.notify({
        message: `確認刪除 ${cmd} 命令集？`, progress: true,
        actions: [
          {
            label: "確認", color: "yellow", icon: "check", handler: () => {
              this.$loadRecord("cmd-list").then(record => {
                delete record[cmd]
                return this.$saveRecord("cmd-list", record)
              }).then(res => {
                this.$q.notify({
                  message: `刪除 ${cmd} 命令集成功`,
                  position: "top", type: "positive",
                })
              }).catch(err => {
                this.$q.notify({
                  message: `刪除 ${cmd} 命令集錯誤 : `,
                  position: "top", err, type: "negative",
                })
              })
            }
          },
          { label: "取消", color: "white", icon: "close" }
        ]
      })
    },
  },
  computed: {
    // 可顯示的 COM 清單
    comList: function () {
      return this.menu.com.list.map((com) => {
        let data = {
          ...com,
          exist: false
        }

        if (this.isCardExisting(com.path)) {
          data.exist = true
        }

        return data
      })
    },

    // 目前已新增卡片
    cardVal: function () {
      return this.cards.list
    },


  }
})

/*window.addEventListener('keydown', (e) => {
  console.log("-------------- keydown ---------------")
  console.log("code    : ", e.code)
  console.log("key     : ", e.key)
  console.log("keyCode : ", e.keyCode)

  switch (event.keyCode) {
    case 96: {  // 0
      break;
    }

    case 97: {  // 1
      break;
    }

    case 98: {  // 2 
      break;
    }

  } // switch
})//*/


// --- Vue 擴充 ---

// 儲存記錄
Vue.prototype.$saveRecord = (key, data) => {
  let self = Vue.prototype

  if (baseFcn.isWeb()) return

  if (!global || !global.JsonStorage)
    return self.$q.notify({
      type: "negative", position: "top",
      message: `JsonStorage 模組載入異常`
    })

  return new Promise((resolve, reject) => {
    global.JsonStorage.set(key, data, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}

// 讀取記錄
Vue.prototype.$loadRecord = (key) => {
  let self = Vue.prototype

  if (baseFcn.isWeb()) return

  if (!global || !global.JsonStorage)
    return self.$q.notify({
      type: "negative", position: "top",
      message: `JsonStorage 模組載入異常`
    })

  return new Promise((resolve, reject) => {
    global.JsonStorage.get(key, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}

// 刪除指定紀錄
Vue.prototype.$removeRecord = (key) => {
  let self = Vue.prototype

  if (baseFcn.isWeb()) return

  if (!global || !global.JsonStorage)
    return self.$q.notify({
      type: "negative", position: "top",
      message: `JsonStorage 模組載入異常`
    })

  return new Promise((resolve, reject) => {
    global.JsonStorage.remove(key, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}

// 清除所有紀錄
Vue.prototype.$clearRecord = (key) => {
  let self = Vue.prototype

  if (baseFcn.isWeb()) return

  if (!global || !global.JsonStorage)
    return self.$q.notify({
      type: "negative", position: "top",
      message: `JsonStorage 模組載入異常`
    })

  return new Promise((resolve, reject) => {
    global.JsonStorage.clear(key, (err) => {
      if (err) return reject(err)

      self.$q.notify({
        type: "positive", position: "top",
        message: `清除所有紀錄`
      })

      resolve()
    })
  })
}