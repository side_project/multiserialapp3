
import baseFcn from '../../script/asset/base-fcn.js'

export default {
  props: {
    path: {
      type: String,
      default: ""
    },

    sendNumData: {  // 是否發送數值資料
      type: Boolean,
      default: false
    },
  },
  data: function () {
    return {
      status: {             // 狀態
        isLoadingStoreCmd: false,
        isLoadCmd: false,         // 識別是載入命令，還是使用者修改命令，判斷是否要自動存檔
      },

      cmdList: [],          // 目前儲存的命令

      cmd: {
        saveAsName: "",     // 另存紀錄名稱
        loadFileName: "",   // 選擇載入檔名
        item: {             // cmd 項目
          value: "",
          description: "",  // cmd 說明
          timestamp: 0,
          mode: {
            type: "",     // "interval", "timeout", "chain"
            timer: null,
            option: {
              interval: 1000,
              timeout: 1000,
              chain: -1,
            }
          },
        },
        list: [],           // 全部共 12 個
      },

      debounce: {
        autoSaveCmd: null
      },
    }
  },
  created: function () {
    // 生成 12 個 cmd 項目
    for (let i = 0; i < 12; i++)
      this.cmd.list.push(Object.copy(this.cmd.item))

    if (baseFcn.isWeb()) return

    // 生成 debounce
    this.debounce.autoSaveCmd = baseFcn.debounce((cmd) => {
      console.log(`${this.path} cmds autoSave`)
      this.saveCmd(`${this.path}-cmds-auto`, cmd)
    }, 1000)

    // 自動載入之前紀錄
    this.loadCmd(`${this.path}-cmds-auto`)
  },
  watch: {
    "cmd.list": {
      handler: function (cmd) {
        if (baseFcn.isWeb()) return

        // 載入命令紀錄檔，不自動儲存
        if (this.status.isLoadCmd) return
        this.debounce.autoSaveCmd(cmd)
      },
      deep: true
    },
  },
  methods: {
    // emit 發送 event
    emitCmd: function (cmd) {
      cmd.timestamp = new Date() / 1
      this.$emit("send", cmd.value)
    },
    // 發送命令
    sendCmd: function (cmd, cmdStatus = null) {
      //console.log("[ sendCmd ] ", cmd)

      if (cmdStatus && cmdStatus.status === "err")
        return this.$emit("msg", cmdStatus.msg)

      switch (cmd.mode.type) {
        case "": { // 沒有啟用發送功能
          this.emitCmd(cmd)
          return
        }

        case "interval": {
          // 已經建立物件，取消
          if (cmd.mode.timer !== null) {
            clearInterval(cmd.mode.timer)
            cmd.mode.timer = null
            return
          }

          cmd.mode.timer = setInterval(() => {
            this.emitCmd(cmd)
          }, cmd.mode.option.interval)
          return
        }

        case "timeout": {
          if (cmd.mode.timer !== null) {
            clearTimeout(cmd.mode.timer)
            cmd.mode.timer = null
            return
          }

          cmd.mode.timer = setTimeout(() => {
            this.emitCmd(cmd)
            cmd.mode.timer = null
          }, cmd.mode.option.timeout)
          return
        }

        case "chain": {
          if (cmd.mode.timer !== null) {
            clearTimeout(cmd.mode.timer)
            cmd.mode.timer = null
            return
          }

          this.emitCmd(cmd)

          let chainIndex = parseInt(cmd.mode.option.chain) - 1
          cmd.mode.timer = setTimeout(() => {
            this.sendCmdByIndex(chainIndex)
            cmd.mode.timer = null
          }, cmd.mode.option.timeout)
          return
        }

        default: {
          console.error(`[ ${this.path} sendCmd ] 未定義的發送功能 : ${cmd.mode.type}`)
        }
      }
    },
    // 指定 index 發送命令
    sendCmdByIndex: function (index) {
      //console.log("[ sendCmdByIndex ] : ", index);

      if (index < 0 || index > 11)
        return { status: "err", msg: "index 範圍請在 0 至 11 之間" }

      let cmd = this.cmd.list[index]
      let cmdStatus = this.cmdStatus[index]
      this.sendCmd(cmd, cmdStatus)
    },

    // 另存命令
    saveAs: function () {
      let fileName = this.cmd.saveAsName
      if (fileName === "")
        return { status: "err", msg: "檔名不可為空" }

      this.saveCmd(fileName, this.cmd.list).then(() => {
        this.$emit("msg", `另存 ${fileName} 成功`)
      }).catch(err => {
        this.$emit("msg", `另存 ${fileName} 失敗 ： ${err}`)
      })
    },

    // 儲存命令
    saveCmd: function (fileName = "", cmds = {}) {
      if (fileName === "")
        return { status: "err", msg: "檔名不可為空" }

      return new Promise((resolve, reject) => {
        this.$loadRecord("cmd-list").then(cmdList => {
          cmdList[fileName] = cmds
          return this.$saveRecord("cmd-list", cmdList)
        }).then(() => {
          resolve()
        }).catch(err => {
          reject(err)
          console.error(`[ ${this.path} Card saveCmd ] 儲存命令清單發生錯誤 : `, err)
        })
      })
    },
    // 讀取指定檔案命令
    loadCmd: function (fileName) {
      if (fileName === "")
        return this.$emit("msg", "檔名不可為空")

      this.status.isLoadCmd = true
      this.$loadRecord("cmd-list").then(cmdList => {
        setTimeout(() => {
          this.status.isLoadCmd = false
        }, 500)

        let targetCmd = cmdList[fileName]

        if (!targetCmd)
          return this.$emit("msg", `${fileName} 命令紀錄檔不存在`)

        this.cmd.list.forEach((cmd, i) => {
          targetCmd[i].mode.timer = null

          cmd.value = targetCmd[i].value
          cmd.description = targetCmd[i].description
          cmd.mode = targetCmd[i].mode
        })

        this.$emit("msg", `${fileName} 命令紀錄檔載入成功`)
      })
    },


    // 讀取已儲存的命令清單
    loadCmdList: function () {
      this.status.isLoadingStoreCmd = true
      this.cmdList.length = 0
      this.cmd.loadFileName = ""

      if (baseFcn.isWeb()) {
        setTimeout(() => {
          this.status.isLoadingStoreCmd = false
        }, 1000)
      }

      this.$loadRecord("cmd-list").then(cmdList => {
        for (let cmdKey in cmdList) {
          this.cmdList.push(cmdKey)
        } // for

        this.status.isLoadingStoreCmd = false
      }).catch(err => {
        console.error(`[ ${this.path} Card ] 讀取儲存命令清單發生錯誤 : `, err)
        this.status.isLoadingStoreCmd = false
      })
    },
  },
  computed: {
    // textarea 顯示接收資料
    textareaString: function () {
      return this.receiceData.map(data => data.data).join("");
    },

    // 命令狀態：是否合理
    cmdStatus: function () {
      return this.cmd.list.map(cmd => {
        // 純數值發送
        if (this.sendNumData) {
          // 檢查數值是否合理
          let { status, msg } = baseFcn.checkSpaceStrArray(cmd.value)
          return { status, msg }
        }

        return { status: "", msg: "" }
      })
    },
  }
}
