// 宣告執行用物件
const gulp = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css')
const connect = require('gulp-connect');
const plumber = require('gulp-plumber');
const webpack = require('webpack-stream');
const rename = require("gulp-rename");
const named = require('vinyl-named');
const autoprefixer = require('gulp-autoprefixer');

const VueLoaderPlugin = require('vue-loader/lib/plugin')


// 指定作業名稱「pug」
gulp.task('pug', function () {
  return gulp.src(['src/pug/*.pug']) // 抓取資料夾中全部pug檔
    .pipe(plumber())
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest('public/'))
});

gulp.task('pug2vue', function () {
  return gulp.src(['src/pug/component/*.pug'])
    .pipe(plumber())
    .pipe(pug({ pretty: true }))
    .pipe(rename({ extname: ".vue" }))
    .pipe(gulp.dest('src/component/'))
});


gulp.task('vueCSS', function () {
  return gulp.src(['src/sass/component/*.sass'])
    .pipe(plumber())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer({
      cascade: true, remove: true
    }))
    //.pipe(gulp.dest('src/component/css'))
    .pipe(concat('component.css'))
    .pipe(minifyCSS({
      keepBreaks: true,
    }))
    .pipe(gulp.dest('public/css/'))
});

gulp.task('vueJS', function () {
  return gulp.src(['src/script/component/*.js'])
    .pipe(plumber())
    .pipe(gulp.dest('src/component/js/'))
});

// 指定作業名稱「sass」
gulp.task('sass', function () {
  return gulp.src(['src/sass/*.sass', '!src/sass/var.sass'])
    .pipe(plumber())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer({
      cascade: true, remove: true
    }))
    .pipe(minifyCSS({
      keepBreaks: true,
    }))
    .pipe(gulp.dest('public/css/'))
});

// 指定作業名稱「script」
gulp.task('script', function () {
  return gulp.src(["src/script/*.js", "!src/script/asset/*.js"
    , "!src/script/screen.js"])
    .pipe(plumber())
    .pipe(gulp.dest('public/js/'))
});//*/


gulp.task('webpack', function () {
  return gulp.src('src/script/index.js')
    .pipe(named())
    .pipe(plumber())
    .pipe(webpack({
      target: "electron-renderer",
      optimization: {
        minimize: false,
      },
      externals: {
        "serialport": "require('serialport')",
        "@serialport/parser-inter-byte-timeout": "require('@serialport/parser-inter-byte-timeout')",
      },
      module: {
        rules: [{
          test: /\.vue$/,
          use: 'vue-loader'
        }, {
          test: /\.js$/,
          loader: "babel-loader",
          exclude: /node_modules/
        }]
      },
      plugins: [
        new VueLoaderPlugin()
      ],
      mode: "production"
    }))
    .pipe(gulp.dest('public/js/'))
    .pipe(connect.reload()); // 更新伺服器
});


// 指定作業名稱「server」
gulp.task('server', function () {
  // 啟用伺服器，到 http://localhost:8080/public/ 即可查看網頁
  connect.server({
    livereload: true,
    port: 8080
  });
});//*/

// 編譯 vue component
gulp.task('component', gulp.series("pug2vue", "vueCSS", "vueJS", "webpack"));

// 指定作業名稱「default」，此作業會呼叫 [ ] 中的作業
gulp.task('default', gulp.series('pug', 'sass', 'script', 'component', 'server'));

gulp.watch('src/sass/**', gulp.series('sass', 'component'));
gulp.watch('src/pug/**', gulp.series('pug', 'component'));
gulp.watch('src/script/**', gulp.series('script', 'component'));